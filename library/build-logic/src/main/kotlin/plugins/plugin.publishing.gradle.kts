import com.vanniktech.maven.publish.KotlinMultiplatform
import com.vanniktech.maven.publish.SonatypeHost
import java.net.URI
import java.util.Properties

plugins {
    id("com.vanniktech.maven.publish")
}

mavenPublishing {
    coordinates("dev.toastbits", "spms", extra["project.version"] as String)

    publishToMavenCentral(SonatypeHost.CENTRAL_PORTAL)

    configure(KotlinMultiplatform(
        sourcesJar = true
    ))

    pom {
        name.set("spmp-server")
        description.set("SpMs (short for spmp-server) is the desktop server component for SpMp")
        url.set("https://gitlab.com/toasterofbread/spmp-server")
        inceptionYear.set("2023")

        licenses {
            license {
                name.set("GPL-3.0")
                url.set("https://www.gnu.org/licenses/gpl-3.0.html")
            }
        }
        developers {
            developer {
                id.set("toasterofbread")
                name.set("Talo Halton")
                email.set("talohalton@gmail.com")
                url.set("https://gitlab.com/toasterofbread")
            }
        }
        scm {
            connection.set("https://github.com/toasterofbread/spmp-server.git")
            url.set("https://gitlab.com/toasterofbread/spmp-server")
        }
        issueManagement {
            system.set("Gitlab")
            url.set("https://gitlab.com/toasterofbread/spmp-server/issues")
        }
    }
}

publishing {
    repositories {
        maven {
            val localProperties: Properties = Properties()
            val localPropertiesFile: File = rootProject.file("local.properties")
            if (localPropertiesFile.isFile) {
                localProperties.load(localPropertiesFile.reader())
            }

            val projectId: String =
                System.getenv("CI_PROJECT_ID") ?: localProperties["gitlab.project.id"] as String

            name = "GitLab"
            url = URI("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
            credentials(PasswordCredentials::class) {
                username = System.getenv("CI_DEPLOY_USER") ?: localProperties["gitlab.deploy.user"] as String?
                password = System.getenv("CI_DEPLOY_PASSWORD") ?: localProperties["gitlab.deploy.key"] as String?
            }
        }
    }
}
